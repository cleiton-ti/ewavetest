import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { FormularioLivroComponent } from './pages/livro/formulario-livro/formulario-livro.component';
import { ListaLivrosComponent } from './pages/livro/lista-livros/lista-livros.component';
import { StoreComponent } from './pages/store/store.component';


const routes: Routes = [
  { path: '',component: StoreComponent},
  { path: 'livros',component: ListaLivrosComponent},
  { path: 'livros/novo',component: FormularioLivroComponent},
  { path: 'livros/:id',component: FormularioLivroComponent},
  { path: 'store',component: StoreComponent},
  // { path: 'store/:id',component: StoreComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
