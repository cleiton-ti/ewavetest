import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor(
    private router: Router
  ) { }

  successSend(uri) {
    Swal.fire({
      title: 'Concluido',
      text: 'Dados enviados com sucesso!',
      icon: 'success',
    }).then((result) => {
      this.router.navigate([uri]);
    });
  }


  erroRequest() {
    Swal.fire({
      title: 'Ooops!',
      text: 'Ocorreu um erro inesperado durante o processamento das informações!',
      icon: 'error',
      showConfirmButton: false,
      showCancelButton: true,
      cancelButtonText: 'Entendi'
    }).then((result) => {
      
    });
  }
}
