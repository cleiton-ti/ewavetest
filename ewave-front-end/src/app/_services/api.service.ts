import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private utils: any;
  private token: String;

  constructor(
    private http: HttpClient
  ) { }

  // Cliente
  getLivros(p) { return this.getAll('livros', p).pipe(map(data => { return data })); }
  getLivro(id) { return this.getById('livros', id).pipe(map(data => { return data })); }
  updateLivro(data) { return this.update('livros', data).pipe(map(data => { return data })); }
  createLivro(data) { return this.create('livros/novo', data).pipe(map(data => { return data })); }
  removeLivro(id) { return this.remove('livros', id).pipe(map(data => { return data })); }

  // API methods

  getById(uri, id) {
    return this.http.get<any>(
      `${environment.apiUrl}/`
      + uri + '/' + id);
  }

  update(uri, data) {
    return this.http.put<any>(
      `${environment.apiUrl}/` + uri + '/' + data.id,
      JSON.stringify(data),
      this.getPostHttpOptions()
    );
  }

  remove(uri, id) {
    return this.http.delete<any>(
      `${environment.apiUrl}/` + uri + '/' + id,
      this.getPostHttpOptions()
    );
  }

  create(uri, data) {
    return this.http.post<any>(
      `${environment.apiUrl}/` + uri,
      JSON.stringify(data),
      this.getPostHttpOptions());
  }

  getAll(uri, param) {
    return this.http.get<any>(
      `${environment.apiUrl}/`
      + uri + this.getQuery(param));
  }

  getPostHttpOptions = () => {
    return {
      headers: new HttpHeaders({
        // 'Request-Method:' : 'PUT',
        // 'Access-Control-Allow-Origin': '*',
        // 'Content-Type': 'application/x-www-form-urlencoded'
        'Content-Type': 'application/json'
        // 'Content-Type': 'text/plain',

      })
    };
  }

  JsonPrepareForPost(obj) {
    let count = 0;
    let result = '';
    for (let key in obj) {
      if (obj[key] == null) continue;
      if (count < 1) {
        result += key + '=' + obj[key];
      } else {
        result += '&' + key + '=' + obj[key];
      }
      count++;
    }
    return result;
  }

  getQuery(obj) {
    let result = '';
    let n =0;
    for (let key in obj) {
      if (obj[key] != '' && obj[key] != null && obj[key] != 'null' && obj[key] != 'undefined')
        result +=  n > 0 ? '&' : '?';
        result += key + '=' + obj[key];
        n++
    }
    return result;
  }

}
