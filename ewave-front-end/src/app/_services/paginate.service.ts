import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaginateService {
  public current_page: number = 0;
  public from: number = 0;
  public last_page: number = 0;
  public per_page: number = 0;
  public to: number = 0;
  public total: number = 0;
  public first_page_url: String = null;
  public last_page_url: String = null;
  public next_page_url: String = null;
  public path: String = null;
  public prev_page_url: String = null;

  constructor() {}

  setData(data){
    this.current_page = data.current_page;
    this.from = data.from;
    this.last_page = data.last_page;
    this.per_page = data.per_page;
    this.to = data.to;
    this.total = data.total;
    this.first_page_url = data.first_page_url;
    this.last_page_url = data.last_page_url;
    this.next_page_url = data.next_page_url;
    this.path = data.path;
    this.prev_page_url = data.prev_page_url;
  }
 
}
