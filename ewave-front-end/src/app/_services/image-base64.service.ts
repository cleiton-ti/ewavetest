import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageBase64Service {

  private base64textString: String = null;
  private timerLoop: Number = 0;

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  handleFileSelect(evt): Observable<String> {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);

      this.timerConversion();

      alert(this.base64textString);

      return of(this.base64textString);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
  }

  timerConversion() {
    if (this.base64textString == null && this.timerLoop <= 6) {
      // this.timerLoop = this.timerLoop.valueOf() + 1;
      setTimeout(() => {
        alert('2222');
        this.timerConversion();
      }, 1000);
    }
  }


  //Call this method in the image source, it will sanitize it.
  transform(imageHash) {
    let base64Image = 'data:image/png;data:image/svg;base64,' + imageHash;
    return this.sanitizer.bypassSecurityTrustResourceUrl(base64Image);
  }
}
