import { TestBed } from '@angular/core/testing';

import { ImageBase64Service } from './image-base64.service';

describe('ImageBase64Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImageBase64Service = TestBed.get(ImageBase64Service);
    expect(service).toBeTruthy();
  });
});
