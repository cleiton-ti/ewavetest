import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/_services/api.service';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { SweetAlertService } from 'src/app/_services/sweet-alert.service';

@Component({
  selector: 'app-lista-livros',
  templateUrl: './lista-livros.component.html',
  styleUrls: ['./lista-livros.component.css']
})
export class ListaLivrosComponent implements OnInit {

  dataList: any = [];

  constructor(
    private apiService: ApiService,
    private sweetAlert: SweetAlertService
  ) { }

  ngOnInit() {
    this.getData({});
  }


  getData(p) {
    // this.loading = true;
    this.apiService.getLivros(p)
      .pipe(first())
      .subscribe(
        data => {
          this.dataList = data;
        },
        error => {
          this.sweetAlert.erroRequest();
        });
  }

  confirmDelete(data) {
    Swal.fire({
      title: 'Deseja remover esse item?',
      text: "Esta ação não poderá ser revertida",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sim, remover!'
    }).then((result) => {
      if (result.value) {
        this.remove(data);
      }
    })
  }

  remove(data) {
    // this.loading = true;
    this.apiService.removeLivro(data.id)
      .pipe(first())
      .subscribe(
        data => {
          Swal.fire(
            'Removido!',
            'Item removido com sucesso.',
            'success'
          )
          this.getData({});
        },
        error => {
          this.sweetAlert.erroRequest();
        });
  }



}
