import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/_services/api.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { SweetAlertService } from 'src/app/_services/sweet-alert.service';
import { ImageBase64Service } from 'src/app/_services/image-base64.service';
import { SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-formulario-livro',
  templateUrl: './formulario-livro.component.html',
  styleUrls: ['./formulario-livro.component.css']
})
export class FormularioLivroComponent implements OnInit {

  id: Number;
  data: any = {};
  urlret: String = '/livros';
  decodedImage: SafeResourceUrl;

  blockSubmit: Boolean = false;
  isLoading = false;

  constructor(
    private apiService: ApiService,
    private sweetAlert: SweetAlertService,
    private route: ActivatedRoute,
    private imageBase64: ImageBase64Service
  ) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    if (this.id > 0) {
      this.getData(this.id);
    }
  }
  getData(id) {
    this.apiService.getLivro(id)
      .pipe(first())
      .subscribe(
        data => {
          this.data = data;
          this.decodeImage();
        },
        error => {
          this.sweetAlert.erroRequest();
        });
  }

  onSubmit() {
    if (this.blockSubmit) {
      return;
    }

    this.blockSubmit = true;

    if (this.id > 0) {
      this.update();
    } else {
      this.create();
    }
  }

  update() {
    this.apiService.updateLivro(this.data)
      .pipe(first())
      .subscribe(
        data => {
          this.sweetAlert.successSend(this.urlret);
        },
        error => {
          this.sweetAlert.erroRequest();
          this.blockSubmit = false;
        });
  }

  create() {
    this.apiService.createLivro(this.data)
      .pipe(first())
      .subscribe(
        data => {
          this.sweetAlert.successSend(this.urlret);
        },
        error => {
          this.sweetAlert.erroRequest();
          this.blockSubmit = false;
        });
  }

  encodeImage(evt) {
    this.handleFileSelect(evt);
  }

  decodeImage() {
    if (this.data.capa != null && this.data.capa != '') {
      this.decodedImage = this.imageBase64.transform(this.data.capa);
    }
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];

    if (files && file) {
      var reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.data.capa = btoa(binaryString);
    this.decodeImage();
  }


}
