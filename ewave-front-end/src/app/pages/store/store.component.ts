import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/_services/api.service';
import { first } from 'rxjs/operators';
import { ImageBase64Service } from 'src/app/_services/image-base64.service';
import { PaginateService } from 'src/app/_services/paginate.service';
import { SweetAlertService } from 'src/app/_services/sweet-alert.service';

declare const openModal: any;
declare const activeModal: any;

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  dataList: any = [];
  detail: any = [];
  filter: String;

  constructor(
    private apiService: ApiService,
    private imageBase64: ImageBase64Service,
    public paginate: PaginateService,
    private sweetAlert: SweetAlertService
  ) { }

  ngOnInit() {
    activeModal();
    this.getData({});
  }

  getData(p) {
    // this.loading = true;
    this.apiService.getLivros(p)
      .pipe(first())
      .subscribe(
        data => {
          this.dataList = data;
        },
        error => {
          this.sweetAlert.erroRequest();
        });
  }

  busca(){
    this.getData({search: this.filter});
  }

  decodeImage(capa) {
    if (capa != null && capa != '') {
      return this.imageBase64.transform(capa);
    }
  }

  openModalInfo(data){
    this.detail = data;
    openModal('modalDetail');
  }

}
