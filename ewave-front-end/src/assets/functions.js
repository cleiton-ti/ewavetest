function dropDownMenuMaterialize() {
  $(".dropdown-trigger").dropdown();
}

function activeSelectMaterialize() {
  $('select').formSelect();
}

function floatBtn() {
  $('.fixed-action-btn').floatingActionButton();
}

function activeModal() {
  $('.modal').modal();
}

function openModal(id) {
  $('#' + id).modal('open');
}

function closeModal(id) {
  $('#' + id).modal('close');
}

function datePicker() {
  var currYear = (new Date()).getFullYear();

  $(".datepicker").datepicker({
    //  defaultDate: new Date(currYear, 1, 31),
    // setDefaultDate: new Date(2000,01,31),
    //maxDate: new Date(currYear - 5, 12, 31),
    // yearRange: [1928, currYear - 5],
    format: "dd/mm/yyyy",
    cancel: "Cancelar",
    months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
    weekdays: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    weekdaysAbbrev: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S']
  });
}

function getDataTo(id) {
  return $('#' + id).val();
}

// function loadJMask() {  
//   $('.money').mask('000.000.000.000.000,00', { reverse: true });
//   $('.cpfMask').mask('000.000.000-00');
//   $('.date').mask('00/00/0000');
//   $('.cep').mask('00000-000');
//   $('.cpf').mask('000.000.000-00');
//   $('.telefone').mask('(00) 0 0000-0000');
// }

function ocultarFiltro() {
  var element = document.getElementById("box-filter");
  element.classList.add("hide");
}

function mostrarFiltro() {
  var element = document.getElementById("box-filter");
  element.classList.remove("hide");
}

// $(document).ready(function () {
//   // Handler for .ready() called.
//   //  M.AutoInit();
//   $('.money').mask('000.000.000.000.000,00', { reverse: true });
// });

