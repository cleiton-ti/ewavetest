# PMV Livraria

MVP criado para avaliação prática.



#### Tecnologias utilizadas

**Front End**
- Angular 8
- Material Angular
- Materialize 1.0
- NodeJs +12
- Sweet Alert 2

**Back End**
- Java 8
- Spring Boot

**Banco de dados**
- MySQL
- H2

# Instalação

**Front End**
**Projeto**: ewave-front

Na pasta do projeto digitar o seguinte comandos:

Para instalação das dependências do projeto
`$ npm install`

Para execução da aplicação em ambiente local. A aplicação será servida localmente por padrão no endereço `localhost:4200`.
`$ ng serve`

Para preparar aplicação para publicação em produção.
Com o comando abaixo será gerada a pasta **dist**, esta por sua vez, deve ser disponibilizada no servidor.
`$ ng build --prod`

**Importante**

Os arquivos `src/environments/environments.*` possuem a variavel global `apiUrl`, esta varial refencia o endereço da api a ser consumida pelo Front End.
Caso a api Rest não esteja sendo servida no endereço `localhost:8080`, será necessário ajustar essas configurações.


**Back End**

Importar projeto na IDE de dua preferência. A aplicação será servida localmente por padrão no endereço `localhost:8080`

- Importar dependências do mavem
- Executar testes
- Ao executar os testes serão criados registros de exemplo
- Executar aplicação

**Back End**

Deve esta configurado servidor MySql para execução da aplicação.

As configurações de conexão podem ser ajustadas no arquivo `src/main/resources/application.properties`.

    spring.jpa.hibernate.ddl-auto=update
    spring.datasource.driverClassName=com.mysql.cj.jdbc.Driver
    spring.datasource.url=jdbc:mysql://localhost:3306/DATABASE_NAME
    spring.datasource.username=USER_DATABASE
    spring.datasource.password=PASSWORD_DATABASE

- Após configuração do banco de dados, na primeira execução da aplicação será criada a estrutura de tabelas.
- Ao executar os testes unitários serão gerados dados para teste da aplicação (Inserção introduzida para facilitar teste do implementado)

### Links Uteis

[Materialize](https://materializecss.com/ "Materialize")

[Material Icons](https://material.io/resources/icons/?style=baseline "Material Icons")

[Material Angular](https://material.angular.io/ "Material Angular")

[Spring Boot](https://spring.io/docs/reference "Spring Boot")

[Sweet Alert](https://sweetalert2.github.io/ "Sweet Alert")


### Autor

Cleiton Oliveira

**Email:** cleiton.ti@outlook.com


### Marco

Aneração para realização de Fork
