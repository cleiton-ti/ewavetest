package com.ewave.store;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ewave.store.beans.Livro;
import com.ewave.store.services.LivroService;

@SpringBootTest
public class FilterLivrosTest {
	
	@Autowired
	private LivroService service;
	
	@Test
	void testFilter(){
		List<Livro> livros =  service.getLivros("desenvolvimento");
		
		if(!livros.isEmpty()) {
			Livro livro = livros.get(livros.size() - 1);
			livro.setDescricao("Test Unit Edit - " + livro.getDescricao());
			
			service.updateLivro(livro);
//			service.deleteLivro(livro.getId());
		}
	}
}
