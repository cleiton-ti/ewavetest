package com.ewave.store.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewave.store.DTO.LivroDTO;
import com.ewave.store.beans.Livro;
import com.ewave.store.repositories.LivroRepository;
import com.ewave.store.services.exceptions.ObjectNotFoundException;

@Service
public class LivroService {
	
	@Autowired
	private LivroRepository repo;
	
	public List<Livro> getLivros(String search){
		List<Livro> livros = repo.findAll(search);
//		LivroDTO dto = new LivroDTO(livros, 20, page+1, titulo, autor, genero);
		return livros;
	}
	
	public Optional<Livro> getLivro(Long id) {
		Optional<Livro> livro = repo.findById(id);
		if(!livro.isPresent()) {
			throw new ObjectNotFoundException("Livro não encontrado! Registro Numero:" + id);
		}
		return livro;
	}
	
	public Livro createLivro(Livro obj) {
		obj.setId(null);
		return repo.saveAndFlush(obj);
	}
	
	public Livro updateLivro(Livro obj) {
		return repo.saveAndFlush(obj);
	}
	
	public void deleteLivro(Long id) {
		repo.deleteById(id);
	}
	
}
