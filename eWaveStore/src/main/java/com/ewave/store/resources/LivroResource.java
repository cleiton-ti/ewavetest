package com.ewave.store.resources;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ewave.store.DTO.LivroDTO;
import com.ewave.store.beans.Livro;
import com.ewave.store.services.LivroService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/livros")
public class LivroResource {

	@Autowired
	private LivroService service;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> all( @RequestParam(required = false) String search ) {
		
		search = search == null ? "" : search;	
		
		List<Livro> livros = service.getLivros(search);
		return ResponseEntity.accepted().body(livros);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Livro> get(@PathVariable Long id) {
		Optional<Livro> livro = service.getLivro(id);
		return ResponseEntity.accepted().body(livro.get());
	}

	@PostMapping(value = "/novo")
	public ResponseEntity<?> create(@RequestBody Livro obj) {
		Livro livro = service.createLivro(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(livro.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping(value = "/{id}")
	private ResponseEntity<?> update(@RequestBody Livro obj, @PathVariable Long id) {
		obj.setId(id);
		obj = service.updateLivro(obj);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping(value = "/{id}")
	private ResponseEntity<?> delete(@PathVariable Long id) {
		service.deleteLivro(id);
		return ResponseEntity.ok().build();
	}

}
