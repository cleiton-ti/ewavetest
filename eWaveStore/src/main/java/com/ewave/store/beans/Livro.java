package com.ewave.store.beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Livro implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	private String titulo;
	private String autor;
	private String generos;
	private LocalDate dataPublicacao;
	private int nrPaginas;
	private String Editora;
	private String descricao;
	
	@Column(columnDefinition = "longtext")
	private String Sinopse;
	
	@Column(columnDefinition = "longtext")
	private String capa;
	
	@Column(columnDefinition = "longtext")
	private String link;
	
	public Livro() {
		super();
	}
	
	public Livro(String titulo, String autor, String generos, LocalDate dataPublicacao, int nrPaginas, String editora, String descricao,
			String sinopse, String capa, String link) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.generos = generos;
		this.dataPublicacao = dataPublicacao;
		this.nrPaginas = nrPaginas;
		Editora = editora;
		this.descricao = descricao;
		Sinopse = sinopse;
		this.capa = capa;
		this.link = link;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGeneros() {
		return generos;
	}

	public void setGeneros(String generos) {
		this.generos = generos;
	}

	public LocalDate getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(LocalDate dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public int getNrPaginas() {
		return nrPaginas;
	}

	public void setNrPaginas(int nrPaginas) {
		this.nrPaginas = nrPaginas;
	}

	public String getEditora() {
		return Editora;
	}

	public void setEditora(String editora) {
		Editora = editora;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSinopse() {
		return Sinopse;
	}

	public void setSinopse(String sinopse) {
		Sinopse = sinopse;
	}

	public String getCapa() {
		return capa;
	}

	public void setCapa(String capa) {
		this.capa = capa;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
