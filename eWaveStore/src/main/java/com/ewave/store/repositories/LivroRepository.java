package com.ewave.store.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ewave.store.DTO.LivroDTO;
import com.ewave.store.beans.Livro;

@Repository
public interface LivroRepository  extends JpaRepository<Livro, Long>{
	
	@Query(nativeQuery = true, 
			value = "select * from livro where "
					+ "(titulo like concat('%', :search, '%') "
					+ "OR autor like concat('%', :search, '%') "
					+ "OR generos like concat('%', :search, '%')) "
					+ " order by id desc")
	public List<Livro> findAll(@Param("search") String search);
	
}
