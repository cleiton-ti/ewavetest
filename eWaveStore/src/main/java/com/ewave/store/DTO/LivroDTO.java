package com.ewave.store.DTO;

import java.io.Serializable;
import java.util.List;

import com.ewave.store.beans.Livro;

public class LivroDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<Livro> data;
	private int limit;
	private int page;
	private String titulo;
	private String autor;
	private String genero;

	public LivroDTO(List<Livro> data, int limit, int page, String titulo, String autor, String genero) {
		this.data = data;
		this.limit = limit;
		this.page = page;
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
	}
	
	public List<Livro> getData() {
		return data;
	}
	public void setData(List<Livro> data) {
		this.data = data;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
		
}
